speed = load('data(kmph)/0.txt');

windows_size = 10;
time_diff = 10;

speed_length = length(speed);

t1 = 1:speed_length;
for i=t1,
    speed(i) = speed(i) * (5.0/18.0);
    current = speed(1:i);
    all_mean(i) = mean(current);
    all_std(i) = std(current);
end

for i=1:speed_length-(windows_size-1),
    current_window = speed(i:i+windows_size-1);
    window_mean(i) = mean(current_window);
    window_std(i) = std(current_window);
    
    diff_mean(i) =  all_mean(i+windows_size-1) - window_mean(i);
end

t2 = windows_size:(windows_size) + (length(window_mean)-1);
max(diff_mean);

max_index = find(diff_mean==max(diff_mean));

max_y = 0:speed_length;
max_x1(1:speed_length+1) = max_index + windows_size;
max_x2(1:speed_length+1) = max_index + windows_size/2;

mean_std = std(diff_mean);
mean_stdX = 1:(windows_size) + (length(window_mean)-1);
mean_stdY(t2) = mean_std;

figure;
plot(t1,speed,t1,all_mean,t2,window_mean,'--',t2,diff_mean,max_x1,max_y,max_x2,max_y,mean_stdX,mean_stdY);
xlabel('Time(10 s)');
ylabel('Speed (m/s)'); 
legend ('speed','mean','window_mean','diff_mean','','','aaa'); 
grid on;