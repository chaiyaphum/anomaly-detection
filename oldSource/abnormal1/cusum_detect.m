v_0 = load('normal_1.txt');
windows_size = 10;
time_diff = 10;

[m,n] = size(v_0);

x  = 1:m-1;

for i=1:m,
    v_0(i) = v_0(i) * (5.0/18.0);
end

for i=1:m-1,
    a_0(i) = (v_0(i+1) - v_0(i))/time_diff;
end

cusum_0 = cumsum(a_0);
[m1,n1] = size(cusum_0);

for i=1:(n1-windows_size),
    mean_window(i) = mean(cusum_0(i:i+windows_size-1));
end

x2 = 1:n1-windows_size;
plot(x,cusum_0,x2,mean_window);

xlabel('Time(20 s)');
ylabel('Acceleration (m/s^2)'); 
title('Graph of X^2'); 
legend ('cusum0'); 
grid on;