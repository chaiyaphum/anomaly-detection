v1 = load('normal_1.txt');
time_diff = 20;

[m,n] = size(v1);
x  = 1:m-1;

for i=1:m,
    v1(i) = v1(i) * (5.0/18.0);
end

for i=1:m-1,
    a1(i)=(v1(i+1)-v1(i))/time_diff;
end

 hmax2 = dsp.Maximum;
 hmax2.RunningMaximum = true;
m1 = step(hstd2,a1)

plot(x,a1,x,m1);

xlabel('Time(20 s)');
ylabel('Acceleration (m/s^2)'); 
title('Graph of X^2'); 
legend ('a1','a2'); 
grid on;