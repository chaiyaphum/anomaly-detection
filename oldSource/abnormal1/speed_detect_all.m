speed = load('0.txt');

windows_size = 18;
time_diff = 10;

speed_length = length(speed);

t1 = 1:speed_length;
for i=t1,
    speed(i) = speed(i) * (5.0/18.0);
end
windows_mean = zeros;

t2 = 1:speed_length-windows_size+1;
for i=t2,
    current_window = speed(i:i+windows_size-1);
    windows_mean(i-1+(windows_size/2)) = mean(current_window);
    windows_sd(i-1+(windows_size/2)) = std(current_window);
end

t2 =1:speed_length-windows_size+(windows_size/2);
plot(t1,speed,t2,windows_mean,t2,windows_sd);

xlabel('Time(10 s)');
ylabel('Speed (m/s)'); 
legend ('speed','mean','sd'); 
grid on;