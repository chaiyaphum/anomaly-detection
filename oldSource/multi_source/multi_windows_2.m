clear
load('ab_8incident.mat')
%load('normal2_8.mat')

round = 5;

for incidentSelect = 1:round ,
    vehicle_matrix = vehicle(:,:,incidentSelect);
    speedM = vehicle_matrix(2,:);

    windows_size = 18;
    time_diff = 10;
    inti = 0;

    speed = [];
    i = 1;
    j = 1;
    speedSum = 0;

    while i<length(speedM),
        if speedM(i) == 0 && speedSum == 0 ,
            inti = inti + 1;
        end

        if speedM(i) > 0 ,
            speed(j) = speedM(i);
            j = j + 1;
        end
        speedSum = speedSum + speedM(i);
        i = i+1;
    end
    
    speed_length = length(speed);
    inti_plot(incidentSelect) = inti;

    t1 = 1:speed_length;
    for i=t1,
        %speed(i) = speed(i) * (5.0/18.0);
        current = speed(1:i);
        all_mean(i) = mean(current);
        all_std(i) = std(current);
    end

    for i=1:speed_length-(windows_size-1),
        current_window = speed(i:i+windows_size-1);
        window_mean(i) = mean(current_window);
        window_std(i) = std(current_window);

        diff_mean(i) =  all_mean(i+windows_size-1) - window_mean(i);
        length(diff_mean);
    end
    
    diff_meanPlot(incidentSelect,1:length(diff_mean)) = diff_mean;
    
    t2 = windows_size:(windows_size) + (length(window_mean)-1);
    max(diff_mean);
    max_index = find(diff_mean==max(diff_mean));

    max_y = 0:speed_length;
    max_x1(1:speed_length+1) = max_index + windows_size-1;
   
    % max_x2 = real point
    max_x2(1:speed_length+1) = (max_index + (windows_size/2))-1;

    mean_std = std(diff_mean);
    mean_stdX = 1:(windows_size) + (length(window_mean)-1);
    mean_stdY(t2) = mean_std;

    plot_time = inti:inti + (length(speed)-1);

    for i=1:speed_length ,
        ab_plot( 1,i,incidentSelect ) = plot_time(i);
        ab_plot( 2,i,incidentSelect ) = speed(i);
        ab_plot( 3,i,incidentSelect ) = max_x1(1);
        ab_plot( 4,i,incidentSelect ) = max_x2(1);
    end
end

%round = round - 1;
cmap = hsv(round);
figure;
for i=1:round ,
    pMatrix = ab_plot(:,:,i);
    r1 = pMatrix(1,1:end);
    r2 = pMatrix(2,1:end);
    r3 = pMatrix(3,1:end);
    r4 = pMatrix(4,1:end);

    x(1:length(r1)) = r1(1):r1+length(r1)-1;
    y(1:length(x)) = i;

    x2(1:length(r1)) = r4(1) + r1(1);
    z2(1:length(x)) = linspace(10,30,length(x));
   
    length(x2);
    length(y);
    length(z2);
    
    %plot3(x,y,r2,x2,y,z2,'Color',cmap(i,:));
    plot3(x,y,r2,x2,y,z2,'Color',cmap(i,:));
    hold on;
    
    xlabel('x')
    ylabel('y')
    zlabel('z')
    grid on
    axis square
end

figure
for i=1:round ,
    mRow = diff_meanPlot(i,:);
    inti_plot(i):length(mRow)+inti_plot(i)
    x_diff(1:length(mRow)) = inti_plot(i):length(mRow)+inti_plot(i)-1
    
    plot(x_diff,mRow,'Color',cmap(i,:));
    xlabel('Time(10 s)');
    ylabel('diff_mean');
    legend ('speed'); 
    grid on;hold on;
end

figure
for i=1:round ,
    mRow = diff_meanPlot(i,:);
    inti_plot(i):length(mRow)+inti_plot(i)
    x_diff(1:length(mRow)) = 1:length(mRow);
    
    plot(x_diff,mRow,'Color',cmap(i,:));
    xlabel('Time(10 s)');
    ylabel('diff_mean');
    legend ('speed'); 
    grid on;hold on;
end