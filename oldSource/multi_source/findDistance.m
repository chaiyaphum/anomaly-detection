function [ distance ] = findDistance( lat1, lon1, lat2, lon2 )
%a = (sin(dlat/2))^2 + cos(lat1) * cos(lat2) * (sin(dlon/2))^2 
%c = 2 * atan2( sqrt(a), sqrt(1-a) ) 
%d = R * c (where R is the radius of the Earth)

% Location 1 = 13.627148,100.42609
% Location 2 = 13.626397,100.44735

    R = 6371; % Radius of the earth in km
    dlat = degtorad(lat2 - lat1);
    dlon = degtorad(lon2 - lon1);
    
    a = power(sin(dlat/2),2) + cos(degtorad(lat1)) * cos(degtorad(lat2)) * power(sin(dlon/2),2);
    c = 2 * atan2(sqrt(a),sqrt(1-a));
    
    distance = (R * c) * 1000;
    
end