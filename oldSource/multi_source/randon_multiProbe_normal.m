%lower = 50 km/h , upper = 120 km/h , diff = 18 km/s-> 5 m/s
speed_lower = 14;
speed_upper = 33;
range = 5;
road_length = 9000;
t = 10;
max_round = 10;

round = 1;
time = 1;

while round <= 8,
    if rand(1) >= 0.60,
        time = time + 1;
        continue;
    end
    
    if round == 1,
        time = 1;
    end
    
    distance = 0;
    index = 1;
    
    intSpeed = 0 + (speed_upper - speed_lower - range).*rand(1);
    
    while distance < road_length,
        vehicle_speed(index) = intSpeed + speed_lower + ((range).*rand(1));
        vehicle_distance(index) = vehicle_speed(index) * t;
        distance = distance + vehicle_distance(index);
        
        vehicle(1,index + (time-1),round) = index * t;
        vehicle(2,index + (time-1),round) = vehicle_speed(index);
        vehicle(3,index + (time-1),round) = vehicle_distance(index);
        index = index + 1;
    end

    round = round + 1;
    time = time + 1;
end

vehicle

round = round - 1;

cmap = hsv(round);
for i=1:round ,
    pMatrix = vehicle(:,:,i);
    r1 = pMatrix(1,1:end);
    r2 = pMatrix(2,1:end);
    r3 = pMatrix(3,1:end);
    
    x = (1:length(r1));
    y(1:length(x)) = i;
    
    plot3(x,y,r2,'Color',cmap(i,:));hold on;
    
    xlabel('x')
    ylabel('y')
    zlabel('z')
    grid on
    axis square      
end