speed = load('6.txt');

windows_size = 18;
time_diff = 10;

mean_windows = zeros;
std_windows = zeros;
u_windows = zeros;
l_windows = zeros;

speed_length = length(speed);

t1 = 1:speed_length;
for i=t1,
    speed(i) = speed(i) * (5.0/18.0);
    current = speed(1:i);
    mean_all(i) = mean(current);
    std_all(i) = std(current);
end

for i=1:speed_length-(windows_size+1),
    current_window = speed(i:i+windows_size-1);
    temp_mean(i) = mean(current_window);
    temp_std(i) = std(current_window);
end

t2 = 1:windows_size+length(temp_mean)-1;
mean_windows(windows_size:windows_size+length(temp_mean)-1) = temp_mean(1:end);
std_windows(windows_size:windows_size+length(temp_std)-1) = temp_std(1:end);

diff_mean(1:windows_size+length(temp_mean)-1) = abs(mean_all(1:windows_size+length(temp_mean)-1)-mean_windows(1:windows_size+length(temp_mean)-1));

figure;
plot(t1,speed,t1,mean_all,t2,mean_windows,t2,diff_mean,t1,std_all);
xlabel('Time(10 s)');
ylabel('Speed (m/s)'); 
legend ('speed','mean','window_mean','diff_mean'); 
grid on;