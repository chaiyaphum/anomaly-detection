
vehicle_matrix = vehicle(:,:,1);
speedM = vehicle_matrix(2,:);

windows_size = 18;
time_diff = 10;
inti = 0;

%speed_length = length(speed);
i = 1;
j = 1;
speedSum = 0;

while i<length(speedM),
    if speedM(i) == 0 && speedSum == 0 ,
        inti = inti + 1;
    end
    
    if speedM(i) > 0 ,
        speed(j) = speedM(i);
        j = j + 1;
    end
    speedSum = speedSum + speedM(i);
    i = i+1;
end

speed_length = length(speed);

t1 = 1:speed_length;
for i=t1,
    %speed(i) = speed(i) * (5.0/18.0);
    current = speed(1:i);
    all_mean(i) = mean(current);
    all_std(i) = std(current);
end

for i=1:speed_length-(windows_size-1),
    current_window = speed(i:i+windows_size-1);
    window_mean(i) = mean(current_window);
    window_std(i) = std(current_window);
    
    diff_mean(i) =  all_mean(i+windows_size-1) - window_mean(i);length(diff_mean);
end

t2 = windows_size:(windows_size) + (length(window_mean)-1);
max(diff_mean);
max_index = find(diff_mean==max(diff_mean));

max_y = 1:speed_length;
max_x1(1:speed_length) = max_index + windows_size-1;
max_x2(1:speed_length) = (max_index + windows_size/2)-1;
max_x3(1:speed_length) = (max_index)-1;

mean_std = std(diff_mean);
mean_stdX = 1:(windows_size) + (length(window_mean)-1);
mean_stdY(t2) = mean_std;

figure;
%plot(t1,speed,t1,all_mean,t2,window_mean,t2,diff_mean,max_x2,max_y,mean_stdX,mean_stdY,max_x1,max_y,max_x3,max_y);
plot(t1,speed,t1,all_mean,t2,window_mean,t2,diff_mean,mean_stdX,mean_stdY);

xlabel('Time(10 s)');
ylabel('Speed (m/s)');
legend ('diff_mean');
legend ('speed','mean','window_mean','diff_mean','diff_std');
%legend ('speed','mean','window_mean');
grid on;

max(diff_mean);
mean_std;